<?php

class TeamController extends BaseController{

	protected $user;
	protected $application;

	public function __construct(UserRepository $user, ApplicationRepository $application){
		$this->user = $user;
		$this->application = $application;
	}

	public function createTeam(){
		if(Auth::user()->team_id)
		{
			Return Redirect::back()->withErrors(['You are already on a team']);
		}
		return View::make('team.create');
	}

	/**
	 * Storing a team
	 * @return [type] [description]
	 */
	public function storeTeam(){
		
		if(Auth::user()->team_id){
			return Redirect::back()->withErrors(['You are already on a team']);
		}

		$input = Input::all();

		$team = Team::create([
			'name' => $input['name']
			]);
		if($team){
			Auth::user()->update(['team_id' => $team_id]);
		}
		return Redirect::to('team/all');
	}

	/**
	 * gets a list of all teams
	 * @return [type] [description]
	 */
	public function getAll(){
		$teams = Team::all();
		return View::make('team.all', compact('teams'));
	}

	/**
	 * join a team
	 * @param  [type] $team_id [description]
	 * @return [type]          [description]
	 */
	public function joinTeam($team_id){


		if(Auth::user()->team_id){

			return Redirect::back()->withErrors(['You are already on a team']);

		}

		if(!$this->application->checkOrCreate(Auth::user()->id,$team_id)){

			return Redirect::back()->withErrors(['You have already applied to that team']);

		}

		return Redirect::back()->with('message', 'Your request has been sent');
	}

	public function getInfo($team_id)
	{
		$team = Team::where('id', $team_id)->with('users')->firstOrFail();

		return View::make('team.info', compact('team'));
	}
}