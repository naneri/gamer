<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('frontend/hello');
	}
	
	public function getIndex(){
		
		$occasions = DB::table('occasions')->where('active', '=', 1)->orderby('occasion_date', 'desc')->take(5)->get();
		$news = DB::table('news')->orderby('created_at', 'desc')->take(5)->get();

		return View::make('frontend/index', array(
			'occasions' => $occasions,
			'news' => $news
			));
	}

}
