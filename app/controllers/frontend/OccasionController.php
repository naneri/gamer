<?php

class OccasionController extends BaseController {
	

	public function getAdd(){
		return View::make('frontend/occasion/add');
	}


    
	public function postCreate(){
		$rules = array('title' => 'required | min:3 | max:255');
        $validator = Validator::make(Input::all(), $rules);
        Input::flash();

        if ($validator->fails()) {
            return Redirect::to('occasion/add')->withErrors($validator)->withInput();
        }

        $occasion = new occasion;
        $occasion->title = Input::get('title');
        $occasion->type = Input::get('type');
        $occasion->address = Input::get('address');
        $occasion->url = Input::get('url');
        $occasion->occasion_date = Input::get('occasion_date');
        $occasion->active = 0;
        $occasion->save();
        return Redirect::to('/');
	}
}
