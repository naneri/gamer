<?php


class TournamentController extends BaseController{

	private $challonge;

	public function __construct(){
		$this->challonge = new ChallongeApi(Config::get('custom.challonge-key'));
	}

	public function getTournaments(){
		$tournaments = $this->challonge->getAllTournaments()->body;
		return View::make('frontend.tournaments.all', compact('tournaments'));
	}

	public function getSingleTournament($id){
		$tournament = $this->challonge->getTournament($id, ['include_participants' => 1])->body->tournament;
		$team = Member::where('user_id', '=', Auth::user()->id)->where('approved', '=', 1)->join('teams', 'members.team_id', '=', 'teams.id')->first();
		return View::make('frontend.tournaments.info', compact('tournament', 'team'));
	}

	public function signup($tournament_id){
		$team = Member::where('user_id', '=', Auth::user()->id)
					->join('teams', 'members.team_id', '=', 'teams.id')
					->select('teams.id', 'teams.name')
					->first();
		$info['participant_name'] = $team->name;
		$tournament = $this->challonge->addParticipant($tournament_id, $info);
		return Redirect::to('tournament/' . $tournament_id);
	}

	public function deleteTournament($id){
		$tournament = $this->challonge->deleteTournament($id);
		return Redirect::to('tournament/all');
	}

	public function createTournament(){
		
	}
}