<?php

class AdminOccasionsController extends BaseController {

	/**
	 * gets Latest five occasions and all their info (including if it is activated or not)
	 * 
	 * @return [type] [description]
	 */
	public function getAll(){
		$occasions = DB::table('occasions')->orderby('occasion_date', 'desc')->take(5)->get();
		return View::make('backend/occasions/all', array(
			'occasions' => $occasions
			));
	}

	public function getActivate(){
		$occasion = Occasion::where('id', '=', Request::segment(4))->update(array('active'=>1));
		return Redirect::to('admin/occasions/all');
	}
}