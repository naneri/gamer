<?php

class AdminNewsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/


	public function getAdd(){
		return View::make('backend/news/add');
	}

	public function postCreate(){
		$rules = array(
			'title' => 'required | min:3 | max:255',
			'content' => 'required | max: 10000'
			);
		$validator = Validator::make(Input::all(), $rules);
        Input::flash();

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $news = new news;
        $news->title = Input::get('title');
        $news->content = Input::get('content');
        $news->author_id = Auth::user()->id;
        $news->save();

        return Redirect::to('admin');
	}
}
