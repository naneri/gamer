<?php

class AuthController extends Controller {


    /**
     * Renders the 'Login' page
     */
	public function getLogin(){
        return View::make('backend/login');
    }


    /**
     * Process the post login data
     */
    public function postLogin(){
       /* $rules = array('email' => 'required', 'password' => 'required');*/
       /* $validator = Validator::make(Input::all(), $rules);*/
        
        /*if($validator->fails()){
            return Redirect::to('login')->withErrors($validator);
        }*/
        
        $user = User::where('email', '=', Input::get('email'))->first();
        $auth = Auth::login($user);
        /*$auth = Auth::attempt(array(
            'email' => Input::get('email')
        ), false);*/
        
        /*if(!$auth){
            return Redirect::to('login')->withErrors(array(
                'Invalid credentials provided'
            ));
        }*/
        
        return Redirect::to('/');
    }
    

    /**
     * Signs out the user
     */
    public function getLogout(){
        Auth::logout();
        return Redirect::to('/');
    }

}
