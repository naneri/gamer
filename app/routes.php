<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
use Evenement\EventEmitter;

Route::get('/', 'HomeController@getIndex');
Route::get('occasion/add','OccasionController@getAdd');
Route::post('occasion/create','OccasionController@postCreate');
Route::get('/login','AuthController@getLogin');
Route::post('/login','AuthController@postLogin');
Route::get('/logout',['as' => 'logout', 'uses' => 'AuthController@getLogout']);
Route::get('/tournament/all', 'TournamentController@getTournaments');
Route::get('/tournament/{id}', 'TournamentController@getSingleTournament');
Route::get('/tournament/delete/{id}', 'TournamentController@deleteTournament');
Route::get('/tournament/create', 'TournamentController@createTournament');
Route::get('/chat/recieve', function(){
	$emiter = new EventEmitter;
	$emiter->emit('message', 'ok');
});

Route::group(['before' => 'auth'], function(){
	Route::get('/team/create', 'TeamController@createTeam');
	Route::post('/team/store', 'TeamController@storeTeam');
	Route::get('team/all', 'TeamController@getAll');
	Route::get('team/join/{id}', 'TeamController@joinTeam');
	Route::get('team/info/{id}', ['as' => 'team/info', 'uses' => 'TeamController@getInfo']);
	Route::get('tournament/{id}/sign-up','TournamentController@signup');
	Route::get('dashboard', array('as' => 'dashboard', 'uses' => 'DashboardController@getIndex'));
});
/**
 * admin routes here...
 */
Route::group(array('before' => array('auth|admin')), function()
{	
	Route::get('admin', 'AdminHomeController@getIndex');
	Route::get('admin/news/add', 'AdminNewsController@getAdd');
	Route::post('admin/news/add', 'AdminNewsController@postCreate');
	Route::get('admin/occasions/all','AdminOccasionsController@getAll');
	Route::get('admin/occasions/activate/{occasion}','AdminOccasionsController@getActivate');
});