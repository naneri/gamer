<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * Checks if the user is admin
	 * 
	 * @return boolean [description]
	 */
	public function isAdmin(){
		if(Auth::user()->is_admin == 1){
			return True;
		}
		return False;
	}

	public function profiles() {
	    return $this->hasOne('Profile');
	}

	public function members(){
		return $this->hasMany('Member');
	}

	public function team()
	{
		return $this->belongsTo('Team');
	}
}
