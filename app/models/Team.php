<?php

class Team extends Eloquent{

	protected $fillable = ['name', 'owner_id'];

	public function users(){
		return $this->hasMany('user');
	}
}