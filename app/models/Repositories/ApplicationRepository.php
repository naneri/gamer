<?php 

class ApplicationRepository
{
    public function checkOrCreate($user_id, $team_id){

        $check = Application
            ::where('user_id', '=', $user_id)
            ->where('team_id', '=', $team_id)
            ->first();

        if($check)
        {
            return False;
        }

        Application::create([
            'team_id'   => $team_id,
            'user_id'   => $user_id,
            'status'    => 'sent',  
            ]);

        return True;
    }
}