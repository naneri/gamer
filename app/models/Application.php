<?php

class Application extends \Eloquent {

	protected $fillable = ['user_id', 'team_id', 'status'];

	public function team(){
		return $this->belongsTo('team');
	}
}