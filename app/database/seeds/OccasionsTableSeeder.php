<?php

class OccasionsTableSeeder extends Seeder{


	public function run(){

		//deletes all info
		DB::table('occasions')->delete();

		// the data
		$occasions = array(
			array(
				'title' => 'Cybersport 1 mix',
				'type' => 'mix',
				'address' => 'Ahunbaeva/Sovetskaia',
				'occasion_date' => '2014-09-14 22:00:00',
				'active' => 1
			),
			array(
				'title' => 'Cybersport 2 mix',
				'type' => 'mix',
				'address' => 'drugaia',
				'occasion_date' => '2014-09-22 10:00:00',
				'active' => 0
			),
			array(
				'title' => 'Cybersport 3 mix',
				'type' => 'champ',
				'address' => 'drugaiaa',
				'occasion_date' => '2014-09-24 08:00:00',
				'active' => 1
			),

		);

		// inserts the data
		DB::table('occasions')->insert($occasions);
	}


}