<?php

class NewsTableSeeder extends Seeder{


	public function run(){

		//deletes all info
		DB::table('news')->delete();

		// the data
		$news = array(
			array(
				'title' => 'Pervaia novsot 1 mix',
				'content' => 'Мы создали первую новость про первый микс. Скоро здесь их будет очень много. ',
				'author_id' => 1
			),
			array(
				'title' => 'Вторая новость',
				'content' => 'Скоро микс бля фывйо. ',
				'author_id' => 1
			),
			array(
				'title' => 'Третья новость1 mix',
				'content' => 'А ещё будет новый микс. ',
				'author_id' => 1
			),

		);

		// inserts the data
		DB::table('news')->insert($news);
	}


}