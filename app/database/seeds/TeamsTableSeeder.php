<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TeamsTableSeeder extends Seeder {

	public function run()
	{
	   DB::table('teams')->delete();

        $faker = Faker::create();

        $users = User::orderByRaw("RAND()")->take(10)->get();

        foreach(range(1, 10) as $index)
        {
        	$user = $users->pop();
        	$team = Team::create([
        		'name' 		=> $faker->company,
        		'created_at'=> $faker->dateTime($max = 'now')
        	]);
            $user->update(['team_id' => $team->id]);
        }
	}

}