@extends('frontend.misc.main')

@section('content')
	@foreach($tournaments as $tournament)
		<p>Name :<a href="{{URL::to('/tournament/' . $tournament->tournament->id)}}"> {{@$tournament->tournament->url}}</a></p>
	@endforeach
@stop