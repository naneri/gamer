@extends('frontend.misc.main')



@section('content')
	<h2>{{$tournament->name}}</h2>
	<p><a href="{{URL::to('tournament/'.$tournament->id.'/sign-up')}}">Register</a></p>
	<p>Participants:</p>
	@foreach($tournament->participants as $participant)
		{{$participant->participant->name}} 
		<hr>
	@endforeach
@stop