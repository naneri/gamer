@extends('frontend.misc.main')

@section('content')
<div class="container">
	<div class='col-md-12'>
		@include('common.includes.alerts')
	</div>
	

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{{ trans('occasion.event.data') }}</h3>
				</div>
				<div class="panel-body">
					{{Form::open(array('url' => 'occasion/create'))}}
						<fieldset>
							<div class="form-group">
								<label for="title">Title</label>
								{{ Form::text('title', Input::old('title', isset($post) ? $post->title : ''), array('class' =>'form-control')) }}
							</div>
							<div class="form-group">
								<label for="type">{{ trans('occasion.type') }}</label>
								{{Form::select('type',array('mix' => 'Mix', 'champ' => 'Champ'),Input::old('type', isset($post) ? $post->type : ''), array('class' =>'form-control'))}}
							</div>
							<div class="form-group">
								<label for="url">{{ trans('occasion.url') }}</label>
								{{ Form::text('url', Input::old('url', isset($post) ? $post->url : ''), array('class' =>'form-control')) }}
							</div>
							<div class="form-group">
								<label for="address">{{ trans('occasion.address') }}</label>
								{{ Form::text('address', Input::old('address', isset($post) ? $post->address : ''), array('class' =>'form-control')) }}
							</div>
							<div class="form-group">
								<label for="date">{{ trans('occasion.event.date') }}</label>
								{{ Form::text('occasion_date', Input::old('occasion_date', isset($post) ? $post->occasion_date : ''), array('class' =>'form-control insert-date')) }}
							</div>
						{{Form::submit('Go!')}}
						</fieldset>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
</div>

	<script>
		$(function() {
		    $( ".insert-date" ).datepicker();
		    $( ".insert-date" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		  });
	</script>
@stop