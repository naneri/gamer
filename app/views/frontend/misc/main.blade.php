@extends('parentlayout')

@section('nested-layout')
@include('frontend.misc.navbar')
   <div class="container"> 
   		@yield('content')
   </div>
@stop