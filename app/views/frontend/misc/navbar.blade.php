<nav class="navbar navbar-default">
	
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a href="{{ URL::to('/')}}">Main</a></li>
			<li><a href="{{ URL::to('occasion/add')}}">Add new occasion</a></li>
			<li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tournaments <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="{{ URL::to('/tournament/all')}}">Get All Tournaments</a></li>
	            <li><a href="{{ URL::to('/tournament/create')}}">Create Tournament</a></li>
	            <li class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	            <li class="divider"></li>
	            <li><a href="#">One more separated link</a></li>
	          </ul>
	        </li>
	        @if(Auth::check())
	        <li class="dropdown">
	        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Teams <span class="caret"></span></a>
	        	<ul class="dropdown-menu" role="menu">
	        		<li><a href="{{ URL::to('/team/all')}}">All Teams</a></li>
            		<li><a href="{{ URL::to('/team/create')}}">Create Team</a></li>	
	        	</ul>
	        </li>	
	        @endif
		</ul>
		<ul class="nav navbar-nav navbar-right">
			@if(!Auth::check())

			<li><a href="{{ URL::to('login')}}">Login</a></li>
			<li><a href="{{ URL::to('login/steam')}}">Login with <i class="fa fa-steam-square"></i></a></li>
			@else
				@if(Auth::user()->is_admin)
					<li><a href="{{ URL::to('admin')}}">Admin</a></li>
				@endif
				<li><a href="#">Hello {{Auth::user()->id}}</a></li>
				<li><a href="{{ URL::route('dashboard')}}">Dashboard</a></li>
				<li><a href="{{ URL::route('logout')}}">Logout</a></li>
			@endif
		</ul>
	</div>
</nav>


@include('common.includes.alerts')