@extends('frontend.misc.main')

@section('content')

<script>
	var user;
	try {
    if (!WebSocket) {
        console.log("no websocket support");
    } else {
    	@if(Auth::user())
        user = {
            id : {{Auth::user()->id}},
            token : '{{Auth::user()->password}}'
        }
        @endif
        var socket = new WebSocket("ws://127.0.0.1:7778/");
        socket.addEventListener("open", function (e) {
            console.log("open: ", e);
        });
        socket.addEventListener("error", function (e) {
            console.log("error: ", e);
        });
        socket.addEventListener("message", function (e) {
            info = JSON.parse(e.data);
            $('.chat').append("<p> "+ info.user.name + ": " + info.text + "</p>");
        });

        
        window.socket = socket;

        socket.send(JSON.stringify({
            type: 'message', 
            user : user,
            text : 'salam'
            
        }));
    }
} catch (e) {
    console.log("exception: " + e);
}
</script>

	<div class="row">
		<div class="col-md-8">
			<div role="tabpanel">
			  <ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Snipe1</a></li>
			    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Snipe2</a></li>
			  </ul>
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="home">
					<object type="application/x-shockwave-flash" height="378" width="750" id="live_embed_player_flash" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=snipealot3" bgcolor="#000000">
						<param name="allowFullScreen" value="true" />
						<param name="allowScriptAccess" value="always" />
						<param name="allowNetworking" value="all" />
						<param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" />
						<param name="flashvars" value="hostname=www.twitch.tv&channel=snipealot3&auto_play=false&start_volume=25" />
					</object>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="profile">
					<object type="application/x-shockwave-flash" height="378" width="750" id="live_embed_player_flash" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=snipealot2" bgcolor="#000000">
						<param name="allowFullScreen" value="true" />
						<param name="allowScriptAccess" value="always" />
						<param name="allowNetworking" value="all" />
						<param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" />
						<param name="flashvars" value="hostname=www.twitch.tv&channel=snipealot2&auto_play=false&start_volume=25" />
					</object>
			    </div>
			  </div>

			</div>
			
		</div>
		<div class="col-md-4">
            
			<p><b>Чат</b></p>
            <div class="chat" style="overflow-y:scroll; height:200px;">
                

            </div>
			<table class="table">
				<thead>
					<th>Date</th>
					<th>Title</th>
				</thead>
				<tbody>
					@foreach ($occasions as $occasion) 
					
						<tr>
							<td>{{date('Y-m-d' , strtotime($occasion->occasion_date))}} <br></td>
						@if(!empty($occasion->url)) 
							<td><a href="{{$occasion->url}}">{{$occasion->title}}</a></td>
						@else
							<td>{{$occasion->title}}</td>
						@endif		
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<hr>
	<p>Новости</p>
	@foreach ($news as $news) 
		{{$news->title}} |
		{{$news->content}} |
		{{$news->created_at}} |<br>
	@endforeach
		
@stop