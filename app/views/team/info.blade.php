
@extends('frontend.misc.main')


@section('content')

    <h1>{{$team->name}}</h1>

    <p style="color:red">Members:</p>
    @foreach($team->users as $user)
        {{$user->email}}<br>
    @endforeach
@stop