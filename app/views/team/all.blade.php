@extends('frontend.misc.main')


@section('content')


	@foreach($teams as $team)
		<p>
            <a href="{{route('team/info', $team->id)}}">{{$team->name}} </a>    
            @if(!Auth::user()->team_id)
            |<a href="{{URL::to('team/join/' . $team->id)}}">Join team</a>
            @endif
        </p>
	@endforeach

@stop