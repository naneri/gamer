<div class="container">
    <div class="all-alerts row">
        @foreach ($errors->all() as $error)
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{$error}}
        </div>
        @endforeach
    </div>
</div>

@if (Session::has('message'))
<div class="row">
	<div class="container ">
	    <div class="alert alert-info alert-dismissible" role="alert">
	        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        {{Session::get('message')}}
	    </div>
	</div>
</div>
@endif