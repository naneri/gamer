
@extends('parentlayout')

@section('nested-layout')
    @include('backend.misc.navbar')
    @yield('content')
@stop
