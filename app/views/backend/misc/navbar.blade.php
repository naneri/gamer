<nav class="navbar navbar-default">
	
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a href="{{ URL::to('/')}}">Stream.kg</a></li>
			<li><a href="{{ URL::to('admin')}}">Main</a></li>
			<li><a href="{{ URL::to('admin/news/add')}}">Add news</a></li>
			<li><a href="{{ URL::to('admin/occasions/all')}}">Check occasion</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="{{ URL::to('logout')}}">Logout</a></li>
		</ul>
	</div>
</nav>