@extends('frontend.misc.main')


@section('content')
<div class="container" style='padding-top: 20px'>
	<div class='row'>
		<div class='col-md-12'>
			@include('common.includes.alerts')
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Please sign in</h3>
				</div>
				<div class="panel-body">
					{{Form::open(array('url' => 'login'))}}
					<fieldset>
						<div class="form-group">
							{{ Form::text('email', Input::old('email', isset($post) ? $post->email : ''), array('class' =>'form-control', 'placeholder' => 'email' , 'type' => 'text')) }}
						</div>
						
						{{Form::submit('Go!')}}

					</fieldset>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
</div>

@stop