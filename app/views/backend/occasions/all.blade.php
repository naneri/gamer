@extends('backend.misc.main')

@section('content')

<div class="container">
	<div class="col-md-12">
		@foreach ($occasions as $occasion) 
		{{$occasion->title}} |
		{{$occasion->type}} |
		{{$occasion->address}} |
		{{$occasion->occasion_date}}|
		{{$occasion->active}}|
			@if($occasion->active) 
			item is already active|
			@else
			<a href="{{ URL::to('admin/occasions/activate/' . $occasion->id)}}">Activate</a>
			@endif
			<br>
		@endforeach
	</div>

</div>

@stop