@extends('backend.misc.main')

@section('content')
<div class="col-md-12">
	<div class="container">
		
	{{Form::open(array('url' => 'admin/news/add'))}}
		<div class="form-group">
		<label for="title">title</label>
		{{ Form::text('title', Input::old('title', isset($post) ? $post->title : ''), array('class' =>'form-control')) }}
	</div>
	<div class="form-group">
		<label for="content">email</label>
		{{ Form::textarea('content', Input::old('content', isset($post) ? $post->content : ''), array('class' =>'form-control')) }}
	</div>
	{{Form::submit('Click Me!')}}
	{{Form::close()}}
	</div>
</div>
@stop