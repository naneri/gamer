<?php

class ExampleTest extends TestCase {

	/**
	 * 	Testing Logout
	 */
	public function testLogout(){
		$this->call('Get', 'logout');
		$this->assertRedirectedToRoute('home');
	}
}
