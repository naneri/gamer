<?php 

class TestOccasion extends TestCase{

	/**
	 * Testing "occasion/add" Route
	 */
	public function testOccasionAdd(){
		$this->call('Get', 'occasion/add');
		$this->assertTrue($this->client->getResponse()->isOk());
	}
	
}