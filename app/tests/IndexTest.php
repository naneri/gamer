<?php 

class TestIndex extends TestCase{

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$crawler = $this->client->request('GET', '/');

		$this->assertTrue($this->client->getResponse()->isOk());
		$this->assertViewHas('tops');
		$this->assertViewHas('news');
		$this->assertViewHas('occasions');
	}
}