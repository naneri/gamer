<?php 

class ChallongeApi{

  private $api_key;
  private $headers = array("Accept" => "application/json");
  private $body;

  public function __construct($api_key){
     $this->uni   = new Unirest\Request; 
     $this->uni->verifyPeer(false);
     $this->body["api_key"] = $api_key;
  }

  /**
   * get list of all tournaments available for the user
   * @param  array  $params [description]
   * @return [type]         [description]
   */
  public function getAllTournaments($params = array()) {
    $url = 'https://api.challonge.com/v1/tournaments.json';
      return $this->uni->get($url, $this->headers, $this->body);
    }

    /**
     * get the tournament info
     * @param  [type] $id     [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function getTournament($tour_id, $params = array()){
      $url = 'https://api.challonge.com/v1/tournaments/'.$tour_id.'.json';
      array_push($this->body, $params);
      return $this->uni->get($url, $this->headers, $this->body);
    }

    /**
     * create a new tournament
     * @param  [type] $info [description]
     * @return [type]       [description]
     */
    public function createTournament($info){
      $url = 'https://api.challonge.com/v1/tournaments.json';
      $params = array(
        "tournament[tournament_type]"   => $info['type'], 
        "tournament[name]"        => $info['name'],
        "tournament[url]"       => "thekanatrounre26"
        );
      array_push($this->body, $params);
      return $this->uni->post($url, $this->headers, $this->body);
    }

    /**
     * add a new participant to the tournament
     * @param [type] $info [description]
     */
    public function addParticipant($tour_id, $info){
      $params = array(
        "participant[name]"       => $info['participant_name'],
        );
      array_push($this->body, $params);
      $url = 'https://api.challonge.com/v1/tournaments/'.$tour_id.'/participants.json';
      return $this->uni->post($url, $this->headers, $this->body);
    }

    public function checkParticipant($tour_id, $participant_id){
      $url = 'https://api.challonge.com/v1/tournaments/'.$tour_id.'/participants/'.$participant_id.'/check_in.json';
      return $this->uni->post($url, $this->headers, $this->body);
    }


    public function deleteTournament($tour_id){
      $url = 'https://api.challonge.com/v1/tournaments/'.$tour_id.'.json';
      return $this->uni->delete($url, $this->headers, $this->body);
    }

}

/*
  "state" => "all",
  "include_matches" => "1",
*/